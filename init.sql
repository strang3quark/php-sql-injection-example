-- MySQL dump 10.13  Distrib 8.0.27, for macos11.6 (x86_64)
--
-- Host: 127.0.0.1    Database: empresa
-- ------------------------------------------------------
-- Server version	5.5.5-10.6.4-MariaDB-1:10.6.4+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE SCHEMA IF NOT EXISTS empresa;
USE empresa;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departamento`
(
    `id_departamento` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
    `nome`            varchar(100) DEFAULT NULL,
    `localizacao`     varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id_departamento`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK
TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento`
VALUES (1, 'Contabilidade', 'Lisboa'),
       (2, 'Pesquisa', 'Leiria'),
       (3, 'Vendas', 'Lisboa'),
       (4, 'Fabrico', 'Santarém'),
       (5, 'IT', 'Peniche');
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Temporary view structure for view `despesas_departamento`
--

DROP TABLE IF EXISTS `despesas_departamento`;
/*!50001 DROP VIEW IF EXISTS `despesas_departamento`*/;
SET
@saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `despesas_departamento` AS SELECT
 1 AS `id_departamento`,
 1 AS `nome`,
 1 AS `gastos_mensais`,
 1 AS `gastos_anuais`,
 1 AS `num_funcionarios`*/;
SET
character_set_client = @saved_cs_client;

--
-- Table structure for table `empregado`
--

DROP TABLE IF EXISTS `empregado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empregado`
(
    `id_empregado`       mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    `nome`               varchar(255)   NOT NULL,
    `data_admissao`      date           NOT NULL DEFAULT current_timestamp(),
    `salario`            decimal(10, 2) NOT NULL DEFAULT 0.00,
    `comissao`           decimal(10, 2) NOT NULL DEFAULT 0.00,
    `id_chefe_fk`        mediumint(8) unsigned DEFAULT NULL,
    `id_departamento_fk` smallint(5) unsigned DEFAULT NULL,
    `id_funcao`          smallint(5) unsigned NOT NULL DEFAULT 1,
    PRIMARY KEY (`id_empregado`),
    KEY                  `id_chefe_fk` (`id_chefe_fk`),
    KEY                  `id_departamento_fk` (`id_departamento_fk`),
    KEY                  `id_funcao` (`id_funcao`),
    CONSTRAINT `empregado_ibfk_1` FOREIGN KEY (`id_chefe_fk`) REFERENCES `empregado` (`id_empregado`),
    CONSTRAINT `empregado_ibfk_2` FOREIGN KEY (`id_departamento_fk`) REFERENCES `departamento` (`id_departamento`),
    CONSTRAINT `empregado_ibfk_3` FOREIGN KEY (`id_funcao`) REFERENCES `funcao` (`id_funcao`)
) ENGINE=InnoDB AUTO_INCREMENT=7935 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empregado`
--

LOCK
TABLES `empregado` WRITE;
/*!40000 ALTER TABLE `empregado` DISABLE KEYS */;
INSERT INTO `empregado`
VALUES (7369, 'Rodrigues', '2016-12-10', 655.00, 20.00, 7902, NULL, 2),
       (7499, 'Antunes', '2017-02-20', 1100.00, 300.00, 7698, 3, 3),
       (7521, 'Oliveira', '2017-02-22', 750.00, 500.00, 7698, 3, 3),
       (7566, 'Pimenta', '2012-04-02', 2495.00, 20.00, 7839, NULL, 4),
       (7654, 'Martins', '2018-03-02', 750.00, 1400.00, 7698, 3, 3),
       (7698, 'Bastos', '2011-05-01', 2350.00, 30.00, 7839, NULL, 5),
       (7782, 'Costa', '2014-06-09', 1950.00, 10.00, 7839, NULL, 6),
       (7788, 'Silva', '2016-04-29', 2500.00, 20.00, 7566, 2, 7),
       (7839, 'Reis', '2011-04-01', 4500.00, 10.00, NULL, NULL, 8),
       (7844, 'Batista', '2018-01-03', 1000.00, 0.00, 7698, 3, 3),
       (7876, 'Andrade', '2020-10-04', 695.00, 20.00, 7788, NULL, 2),
       (7900, 'Coelho', '2021-10-04', 675.00, 30.00, 7698, NULL, 2),
       (7902, 'Pereira', '2018-04-29', 2500.00, 20.00, 7566, 2, 7),
       (7934, 'Garcia', '2021-11-01', 800.00, 10.00, 7782, NULL, 2);
/*!40000 ALTER TABLE `empregado` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `funcao`
--

DROP TABLE IF EXISTS `funcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `funcao`
(
    `id_funcao` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
    `nome`      varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id_funcao`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcao`
--

LOCK
TABLES `funcao` WRITE;
/*!40000 ALTER TABLE `funcao` DISABLE KEYS */;
INSERT INTO `funcao`
VALUES (1, 'Nenhuma'),
       (2, 'Administrativo'),
       (3, 'Vendedor'),
       (4, 'CTO'),
       (5, 'CSO'),
       (6, 'CHRO'),
       (7, 'Analista'),
       (8, 'Presidente');
/*!40000 ALTER TABLE `funcao` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `nivel_salarial`
--

DROP TABLE IF EXISTS `nivel_salarial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nivel_salarial`
(
    `id_nivel` tinyint(3) unsigned NOT NULL,
    `min`      decimal(10, 2) NOT NULL,
    `max`      decimal(10, 2) NOT NULL,
    PRIMARY KEY (`id_nivel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivel_salarial`
--

LOCK
TABLES `nivel_salarial` WRITE;
/*!40000 ALTER TABLE `nivel_salarial` DISABLE KEYS */;
INSERT INTO `nivel_salarial`
VALUES (1, 200.00, 700.00),
       (2, 701.00, 900.00),
       (3, 901.00, 1500.00),
       (4, 1501.00, 2500.00),
       (5, 2501.00, 9499.00);
/*!40000 ALTER TABLE `nivel_salarial` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Final view structure for view `despesas_departamento`
--

/*!50001 DROP VIEW IF EXISTS `despesas_departamento`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `despesas_departamento` AS select `d`.`id_departamento` AS `id_departamento`,`d`.`nome` AS `nome`,coalesce(sum(`e`.`salario`),0) AS `gastos_mensais`,coalesce(sum(`e`.`salario`) * 14,0) AS `gastos_anuais`,count(`e`.`id_empregado`) AS `num_funcionarios` from (`departamento` `d` left join `empregado` `e` on(`d`.`id_departamento` = `e`.`id_departamento_fk`)) group by `d`.`id_departamento` union select NULL AS `NULL`,NULL AS `NULL`,sum(`empregado`.`salario`) AS `sum(salario)`,sum(`empregado`.`salario`) * 14 AS `sum(salario) * 14`,count(`empregado`.`id_empregado`) AS `count(id_empregado)` from `empregado` where `empregado`.`id_departamento_fk` is null */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-27 17:30:06
