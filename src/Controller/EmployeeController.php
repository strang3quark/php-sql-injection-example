<?php

namespace App\Controller;

use App\Config\View;
use App\Repository\EmployeeRepository;
use Laminas\Diactoros\ServerRequest;
use Relay\Runner;

class EmployeeController
{

    private EmployeeRepository $repository;
    private View $view;

    public function __construct(EmployeeRepository $repository, View $view)
    {
        $this->repository = $repository;
        $this->view = $view;
    }

    public function __invoke(ServerRequest $request, Runner $runner)
    {
        $filter = "";

        $form = $request->getParsedBody();
        if (is_array($form) && array_key_exists("filter", $form)) {
            $filter = $form["filter"];
        }

        $employees = $this->repository->findEmployee($filter);

        $this->view->file("EmployeeView.php");
        $this->view->content("employees", $employees);
        $this->view->content("filter", $filter);
        $this->view->render();

        return null;
    }


}