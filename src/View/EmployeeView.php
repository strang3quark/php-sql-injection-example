<?php
$container = $container ? $container : [];
$empregados = $container["employees"];
$filter = $container["filter"];

require "Template/Header.php";
?>

<?php
if (isset($empregados) && count($empregados) > 0) {
    ?>
    <div class="row">
        <div class="col-12">
            Encontrados <?= count($empregados) ?> resultados:
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-striped">
                <tr>
                    <th scope='col'>ID</th>
                    <th scope='col'>Nome</th>
                    <th scope='col'>Data de Admissão</th>
                    <th scope='col'>Função</th>
                </tr>
                <?php
                foreach ($empregados as $empregado) {
                    echo "<tr>";
                    foreach ($empregado as $property) {
                        echo "<td>$property</td>";
                    }
                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>

    <?php
}
require "Template/Footer.php";
?>
