<?php

namespace App\Repository;

use App\Config\Database;
use PDO;
use PDOException;

class EmployeeRepository
{
    private Database $database;

    /**
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        $this->database = $database;
    }


    public function findEmployee(string $name): array|bool|null
    {
        try {
            $sql = "SELECT id_empregado as ID, e.nome as Nome, e.data_admissao as Data_Admissao, f.nome as Funcao
                    FROM empregado e
                    INNER JOIN funcao f on e.id_funcao = f.id_funcao
                    WHERE e.nome LIKE '%$name%'";

            $db = $this->database->connection();

            $stmt = $db->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print_r($e->getMessage());
            return null;
        }
    }

    public function findEmployeeFixed(string $name): array|bool
    {
        try {
            $sql = "SELECT id_empregado as ID, e.nome as Nome, e.data_admissao as Data_Admissao, f.nome as Funcao
                    FROM empregado e
                    INNER JOIN funcao f on e.id_funcao = f.id_funcao
                    WHERE e.nome LIKE :name";

            $db = $this->database->connection();

            $stmt = $db->prepare($sql);

            $stmt->bindValue(":name", "%$name%", PDO::PARAM_STR);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return false;
        }
    }
}