<?php

namespace App\Config;

use PDO;

class Database
{
    const DB_HOST = '127.0.0.1';
    const DB_NAME = 'empresa';
    const DB_USER = 'root';
    const DB_PASS = 'root';

    private PDO $dbConnection;

    public function __construct()
    {
        $mysql_connect_str = "mysql:charset=utf8mb4;host=" . self::DB_HOST . ";dbname=" . self::DB_NAME . ";charset=utf8;";
        $this->dbConnection = new PDO($mysql_connect_str, self::DB_USER, self::DB_PASS);
        $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function connection(): PDO
    {
        return $this->dbConnection;
    }
}