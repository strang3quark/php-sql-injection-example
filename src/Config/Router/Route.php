<?php

namespace App\Config\Router;

class Route
{


    private array|string $method;
    private string $route;
    private string $handler;

    public function __construct()
    {
    }

    static function create(): Route
    {
        return new Route();
    }


    /**
     * @param string|string[] $method
     * @return $this
     */
    public function method(array|string $method):self {
        $this->method = $method;
        return $this;
    }

    public function route(string $route): self {
        $this->route = $route;
        return $this;
    }

    public function handler(string $handler): self {
        $this->handler = $handler;
        return $this;
    }
    

    /**
     * @return string
     */
    public function getMethod(): array|string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    public function getHandler(): string
    {
        return $this->handler;
    }

}
