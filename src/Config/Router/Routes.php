<?php

namespace App\Config\Router;

use App\Controller\EmployeeController;

class Routes
{
    /**
     * @return Route[]
     */
    static function get(): array
    {
        return [
            Route::create()
                ->method([HTTPMethod::GET, HTTPMethod::POST])
                ->route('/')
                ->handler(EmployeeController::class)
        ];
    }
}