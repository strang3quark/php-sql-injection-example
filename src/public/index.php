<?php

declare(strict_types=1);

use App\Config\DI\Definitions;
use App\Config\Router\Routes;
use DI\ContainerBuilder;
use FastRoute\RouteCollector;
use Laminas\Diactoros\ServerRequestFactory;
use Middlewares\FastRoute;
use Middlewares\RequestHandler;
use Narrowspark\HttpEmitter\SapiEmitter;
use Relay\Relay;
use function FastRoute\simpleDispatcher;

require_once '../../vendor/autoload.php';

$containerBuilder = new ContainerBuilder();
$containerBuilder->useAutowiring(true);
$containerBuilder->useAnnotations(false);
$containerBuilder->addDefinitions(Definitions::get());

try {
    $container = $containerBuilder->build();
} catch (Exception $e) {
    echo "Cannot initialize container builder";
    die;
}

$dispatcher = simpleDispatcher(function (RouteCollector $r) {
    $routes = Routes::get();

    foreach ($routes as $route) {
        $r->addRoute(
            $route->getMethod(),
            $route->getRoute(),
            $route->getHandler()
        );
    }
});


$middlewareQueue[] = new FastRoute($dispatcher);
$middlewareQueue[] = new RequestHandler($container);

$requestHandler = new Relay($middlewareQueue);
$response = $requestHandler->handle(ServerRequestFactory::fromGlobals());

$emitter = new SapiEmitter();
$emitter->emit($response);